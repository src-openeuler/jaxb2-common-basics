Name:                jaxb2-common-basics
Version:             1.11.1
Release:             1
Summary:             JAXB2 Basics
License:             BSD-2-Clause
Url:                 https://github.com/highsource/jaxb2-basics
Source0:             https://github.com/highsource/jaxb2-basics/archive/refs/tags/%{version}.tar.gz
Patch0:              0001-Port-to-latest-version-of-javaparser.patch
BuildRequires:       maven-local mvn(com.google.code.javaparser:javaparser)
BuildRequires:       mvn(com.vividsolutions:jts) mvn(commons-beanutils:commons-beanutils)
BuildRequires:       mvn(commons-io:commons-io) mvn(javax.xml.bind:jaxb-api) mvn(junit:junit)
BuildRequires:       mvn(org.apache.ant:ant) mvn(org.apache.ant:ant-launcher)
BuildRequires:       mvn(org.apache.commons:commons-lang3) mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:       mvn(org.glassfish.jaxb:jaxb-runtime) mvn(org.glassfish.jaxb:jaxb-xjc)
BuildRequires:       mvn(org.jvnet.jaxb2.maven2:maven-jaxb22-plugin)
BuildRequires:       mvn(org.jvnet.jaxb2.maven2:maven-jaxb2-plugin-testing)
BuildRequires:       mvn(org.slf4j:jcl-over-slf4j) mvn(org.slf4j:slf4j-api)
BuildRequires:       mvn(org.sonatype.oss:oss-parent:pom:)
BuildRequires:       springframework-context mvn(xmlunit:xmlunit)
BuildArch:           noarch
%description
JAXB2 Basics is a part of JAXB2 Commons project which
implements plugins and tools for JAXB 2.x reference
implementation.

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains javadoc for %{name}.

%prep
%setup -q -n jaxb2-basics-%{version}
%patch0 -p1
find -name "*.bat" -print -delete
find -name "*.class" -print -delete
find -name "*.jar" -print -delete
%pom_remove_plugin :maven-source-plugin
%pom_remove_plugin :maven-deploy-plugin plugins
%pom_remove_plugin :maven-shade-plugin plugins
%pom_disable_module dist
%pom_disable_module samples
%pom_disable_module tests

%pom_change_dep :ant-optional org.apache.ant:ant
%pom_change_dep :maven-jaxb2-plugin :maven-jaxb22-plugin
%pom_change_dep -r org.springframework:spring org.springframework:spring-context
%pom_xpath_set "pom:dependency[pom:artifactId = 'tools' ]/pom:groupId" com.sun
%pom_xpath_remove "pom:dependency[pom:artifactId = 'tools' ]/pom:scope"
%pom_xpath_remove "pom:dependency[pom:artifactId = 'tools' ]/pom:systemPath"
%pom_xpath_set "pom:plugin[pom:groupId = 'org.jvnet.jaxb2.maven2' ]/pom:artifactId" maven-jaxb22-plugin
sed -i -e 's/com\.vividsolutions\.jts/org.locationtech.jts/' $(find -name *.java)
sed -i '/a1.b.a, a1/d' runtime/src/test/java/org/jvnet/jaxb2_commons/lang/tests/CyclicTests.java

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%doc README.md TODO.md
%license LICENSE

%files javadoc -f .mfiles-javadoc
%license LICENSE

%changelog
* Tue Apr 30 2024 Ge Wang <wang__ge@126.com> - 1.11.1-1
- Update to version 1.11.1

* Wed Oct 28 2020 baizhonggui <baizhonggui@huawei.com> - 0.9.5-2
- Remove spring-context-support dep

* Wed Aug 5 2020 chengzihan <chengzihan2@huawei.com> - 0.9.5-1
- Package init
